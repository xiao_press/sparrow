<?php

namespace vendor;

class Session
{

    /**
     * Session 默认配置
     * @var array
     */
    protected static $configs = [
        'PREFIX' => 'sparrow_',         // 前缀
        'EXPIRE' => 3600,       // 过期时间
        'TYPE' => 'File',       // 驱动类型
        'PATH' => '/var/app/session'        // SESSION 的存储目录
    ];

    /**
     * Session 实现类命名空间
     * @var string
     */
    private static $typeNamespace = 'vendor\\session\\';

    /**
     * 初始化 Session
     */
    public static function init()
    {
        // 加载配置
        $configs = array_merge(self::$configs, Config::get('SESSION'));
        self::$configs = $configs;
        // 加载驱动
        if($configs['TYPE'] !== 'FILE'){
            $clazz = self::$typeNamespace . $configs['TYPE'];
            session_set_save_handler(new $clazz($configs));
        }
        // 设置 SESSION 的过期时间
        ini_set('session.gc_maxlifetime', $configs['EXPIRE']);
        ini_set('session.cookie_lifetime', $configs['EXPIRE']);
        // 设置 SESSION 的存储目录
        session_save_path($configs['PATH']);    // FixMe: 目录设置不能生效
        // 开启会话
        session_start();
    }

    /**
     * 设置 Session
     * @param string $key
     * @param string $value
     */
    public static function set($key, $value): void
    {
        if(empty(self::$configs['PREFIX'])){
            $_SESSION[$key] = $value;
        } else {
            $_SESSION[self::$configs['PREFIX']][$key] = $value;
        }
    }

    /**
     * 获取 Session
     * @param string $key
     * @return string
     */
    public static function get($key): string
    {
        if(empty(self::$configs['PREFIX'])){
            return $_SESSION[$key];
        } else {
            return $_SESSION[self::$configs['PREFIX']][$key];
        }
    }

    /**
     * 返回 Session 是否存在
     * @param string $key
     * @return bool
     */
    public static function has($key): bool
    {
        if(empty(self::$configs['PREFIX'])){
            return isset($_SESSION[$key]);
        } else {
            return isset($_SESSION[self::$configs['PREFIX']][$key]);
        }
    }

    /**
     * 删除 Session
     * @param string $key
     */
    public static function del($key): void
    {
        if(empty(self::$configs['PREFIX'])){
            unset($_SESSION[$key]);
        } else {
            unset($_SESSION[self::$configs['PREFIX']][$key]);
        }
    }
}