<?php
namespace vendor;

use vendor\cache\CacheInterface;

class Cache
{
    /**
     * @var CacheInterface
     */
    protected static $instance = null;

    /**
     * 缓存实现类命名空间
     * @var string
     */
    protected static $implNameSpace = 'vendor\\cache';

    /**
     * 获取缓存实例
     * @return CacheInterface
     */
    public static function getInstance()
    {
        // 获取缓存组件实例
        if(is_null(self::$instance)){
            // 获取缓存的配置
            $configs = Config::get('CACHE');
            $implClazz = self::$implNameSpace . '\\' . $configs['TYPE'] . 'Impl';
            self::$instance = new $implClazz($configs);
        }

        return self::$instance;
    }
}