<?php
namespace vendor;

class Validator
{

    /**
     * 规则和校验规则的方法的数组映射
     * @var array
     */
    protected static $ruleMethodMap = [
        'required' => [self::class, 'required'],
        'maxLength' => [self::class, 'maxLength'],
        'minLength' => [self::class, 'minLength'],
    ];

    /**
     * 默认的错误提示
     * @var array
     */
    protected static $errorMessageMap = [
        'required' => 'The field \'%s\' is required!',
        'maxLength' => 'The field \'%s\' maxLength is %s!',
        'minLength' => 'The field \'%s\' minLength is %s!',
    ];

    /**
     * 最近的错误
     * @var string
     */
    protected static $lastError = '';

    /**
     * 校验传参
     * @param array $params 参数
     * @param array $keyRules 规则
     * @return bool
     */
    public static function check($params, $keyRules): bool
    {
        // key: name        rules: required|minLength:1|maxLength:10
        foreach($keyRules as $key => $rules){
            // ['required','minLength:1','maxLength:10']
            $rulesArr = explode('|', $rules);
            foreach($rulesArr as $rule){
                // rule: required rule:minLength:1 rule:maxLength:10
                $ruleName = self::getRuleName($rule);
                // 调用校验方法进行校验
                $result = call_user_func(self::$ruleMethodMap[$ruleName], $rule, $key, $params);
                // 如归校验错误则终止其他规则校验
                if(!$result){
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * 获取规则名称
     * @param string $rule
     * @return bool|string
     */
    private static function getRuleName($rule)
    {
        $ruleName = $rule;
        if(strpos($rule, ':')){
            $ruleName = substr($rule, 0, strpos($rule, ':'));
        }

        return $ruleName;
    }

    /**
     * 必填验证
     * @param $rule
     * @param $key
     * @param $params
     * @return bool
     */
    protected static function required($rule, $key, $params): bool
    {
        $result = isset($params[$key]);
        if(!$result){
            $formatErrorMessageStr = self::$errorMessageMap[self::getRuleName($rule)];
            self::$lastError = sprintf($formatErrorMessageStr, $key);
        }

        return $result;
    }

    /**
     * 字符串最小长度验证
     * @param $rule
     * @param $key
     * @param $params
     * @return bool
     */
    protected static function minLength($rule, $key, $params): bool
    {
        $valueLength = mb_strlen($params[$key], 'utf-8');
        $minLength = (explode(':', $rule))[1];

        $result = ($valueLength > $minLength);
        if(!$result){
            $formatErrorMessageStr = self::$errorMessageMap[self::getRuleName($rule)];
            self::$lastError = sprintf($formatErrorMessageStr, $key, $minLength);
        }

        return $result;
    }


    /**
     * 字符串最大长度验证
     * @param $rule
     * @param $key
     * @param $params
     * @return bool
     */
    protected static function maxLength($rule, $key, $params): bool
    {
        $valueLength = mb_strlen($params[$key], 'utf-8');
        $maxLength = (explode(':', $rule))[1];

        $result = ($valueLength < $maxLength);
        if(!$result){
            $formatErrorMessageStr = self::$errorMessageMap[self::getRuleName($rule)];
            self::$lastError = sprintf($formatErrorMessageStr, $key, $maxLength);
        }

        return $result;
    }

    /**
     * 获取最近的错误
     * @return string
     */
    public static function getLastedError()
    {
        return self::$lastError;
    }

}