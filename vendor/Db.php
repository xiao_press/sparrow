<?php

namespace vendor;

use mysqli;

class Db
{

    /**
     * @var null|mysqli
     */
    protected static $instance = null;

    /**
     * 表名
     * @var string
     */
    protected static $table = '';

    protected static $join = '';

    /**
     * SQL 组成部分
     * @var array
     */
    protected static $sql = [
        'fields' => '*',
        'where' => '',
        'group' => '',
        'order' => '',
        'limit' => '',
        'page' => null,
    ];

    /**
     * 默认的数据库配置
     * @var array
     */
    protected static $dbConfigs = [
        'HOST' => 'localhost',
        'USER' => 'root',
        'PASSWORD' => '',
        'NAME' => 'test',
        'PORT' => 3306,
        'CHARSET' => 'utf8mb4',
    ];


    /**
     * 最近执行的 SQL
     * @var string
     */
    protected static $lastSql = '';

    /**
     * Db constructor.
     * 不允许实例化
     */
    protected function __construct(){}

    /**
     * 获取数据库实例
     * @return Db
     * @throws \Exception
     */
    public static function getInstance()
    {
        if(is_null(self::$instance)){
            // 初始化数据库配置
            $dbConfigs = array_merge(self::$dbConfigs, Config::get('DATABASE'));
            // 连接数据库
            self::$instance = new mysqli($dbConfigs['HOST'], $dbConfigs['USER'], $dbConfigs['PASSWORD'],
                $dbConfigs['NAME'], $dbConfigs['PORT']);
            if(!self::$instance->get_connection_stats()){
                Log::notice([
                    'MESSAGE' => 'Database connection failed!',
                    'ERROR NO' => self::$instance->connect_errno,
                    'ERROR MESSAGE' => self::$instance->connect_error,
                    'DB CONFIGS' => $dbConfigs,
                ], 'SYSTEM', Log::TYPE_ERROR);
                throw new \Exception('Database connection failed!');
            }
            // 设置字符集
            self::$instance->set_charset($dbConfigs['CHARSET']);
        }

        return new static;
    }

    /**
     * 设置数据表名
     * @param string $table
     * @return Db
     */
    public static function table($table)
    {
        self::$table = $table;

        return new static;
    }

    /**
     * 设置字段
     * @param string ...$fields
     * @return Db
     */
    public static function fields(...$fields)
    {
        self::$sql['fields'] = implode(',', $fields);

        return new static;
    }

    /**
     * 设置 JOIN
     * @param string $table 表名
     * @param string $on 联表条件
     * @param string $type 类型 [LEFT|RIGHT|INNER]
     * @return Db
     */
    public static function join($table, $on, $type = 'LEFT')
    {
        self::$join .= " $type JOIN " . $table . ' ON ' . $on;

        return new static;
    }

    /**
     * 设置首个条件
     * @param array $conditions [字段，操作符，值]
     * @return Db
     */
    public static function where(array $conditions)
    {
        list($field, $operator, $value) = $conditions;
        !is_numeric($value) && $value = "'$value'";
        self::$sql['where'] .= " ( `$field` $operator $value ) ";

        return new static;
    }

    /**
     * 设置 AND 条件
     * @param array $conditions [字段，操作符，值]
     * @return Db
     */
    public static function andWhere($conditions)
    {
        list($field, $operator, $value) = $conditions;
        !is_numeric($value) && $value = "'$value'";
        self::$sql['where'] .= " AND ( `$field` $operator $value ) ";

        return new static;
    }

    /**
     * 设置 OR 条件
     * @param array $conditions [字段，操作符，值]
     * @return Db
     */
    public static function orWhere($conditions)
    {
        list($field, $operator, $value) = $conditions;
        !is_numeric($value) && $value = "'$value'";
        self::$sql .= " OR ( $field $operator $value ) ";

        return new static;
    }

    /**
     * 设置分组
     * @param string $fields
     * @return Db
     */
    public static function group($fields = '')
    {
        self::$sql['group'] = $fields;

        return new static;
    }

    /**
     * 设置排序
     * @param array $orderBys
     * @return Db
     */
    public static function order($orderBys = [])
    {
        $order = '';
        foreach ($orderBys as $orderBy) {
            list($field, $ascOrDesc) = $orderBy;
            $order .= " $field $ascOrDesc ,";
        }
        $order = substr($order, 0, -1);
        self::$sql['order'] = $order;

        return new static;
    }

    /**
     * 设置获取记录数
     * @param int $limit
     * @return Db
     */
    public static function limit($limit)
    {
        self::$sql['limit'] = $limit;

        return new static;
    }

    /**
     * 设置分页
     * @param int $page
     * @param int $pageSize
     * @return Db
     */
    public static function page($page, $pageSize)
    {
        self::$sql['limit'] = ' ' . ($page * $pageSize + 1) . ' , ' . $pageSize  ;

        return new static;
    }

    /**
     * 获取一条记录
     * @return bool|mixed|\mysqli_result
     */
    public static function find()
    {
        self::$lastSql = $sql = self::buildQuery(true);

        return self::exec($sql);
    }

    /**
     * 获取所有记录
     * @return bool|mixed|\mysqli_result
     */
    public static function all()
    {
        self::$lastSql = $sql = self::buildQuery(false);

        return self::exec($sql);
    }

    /**
     * 构建查询语句
     * @param bool $fetchOne
     * @return string
     */
    protected static function buildQuery($fetchOne = true): string
    {
        $group = $order = $where = $limit = $join = '';
        self::$join && $join = self::$join;
        self::$sql['group'] && $group = ' GROUP BY ' . self::$sql['group'];
        self::$sql['order'] && $order = ' ORDER BY ' . self::$sql['order'];
        self::$sql['where'] && $where = ' WHERE ' . self::$sql['where'];
        $sql = 'SELECT ' . self::$sql['fields'] . ' FROM ' . static::$table
            . " $join " . $where . $group . $order;
        // 判断 Limit 是否为 1
        if($fetchOne){
            return $sql . ' LIMIT 1;';
        } else {
            self::$sql['limit'] && $limit = ' LIMIT ' . self::$sql['limit'];
            return $sql . $limit . ';';
        }
    }


    /**
     * 执行 SQL
     * @param string $sql
     * @return bool|mixed|\mysqli_result
     */
    protected static function exec($sql)
    {
        $result = self::$instance->query($sql);
        $result && $result = $result->fetch_all(MYSQLI_ASSOC);

        return $result;
    }

    /**
     * 插入单行记录
     * @param array $insertData
     * @return bool
     */
    public static function insert(array $insertData)
    {

        $sql = 'INSERT INTO ' . static::$table . ' (`'
            . implode('`,`', array_keys($insertData)) . '`) VALUE (';
        $values = array_values($insertData);
        foreach($values as $value){
            is_string($value) && $value = "'$value'";
            $sql .= $value . ',';
        }
        $sql = mb_substr($sql, 0, -1, 'utf-8') . ');';
        self::$lastSql = $sql;
        self::$instance->query($sql);

        return boolval(self::$instance->affected_rows);
    }

    /**
     * 更新记录
     * @param array $updateData
     * @return int 返回受影响条数
     */
    public static function update(array $updateData)
    {
        $where = '';
        self::$sql['where'] && $where = ' WHERE ' . self::$sql['where'];
        $sql = 'UPDATE ' . static::$table . ' SET ';
        foreach($updateData as $key => $value){
            if(is_string($value)){
                $value = "'$value'";
            }
            $sql .= " `$key` = $value,";
        }
        $sql = mb_substr($sql, 0, -1, 'utf-8') . $where;
        self::$lastSql = $sql;
        self::$instance->query($sql);

        return self::$instance->affected_rows;
    }

    /**
     * 获取最后执行的SQL
     * @return string
     */
    public static function getLastSql()
    {
        return self::$lastSql;
    }

    /**
     * 开启事务
     */
    public static function beginTransaction()
    {
        self::$instance->begin_transaction();
    }

    /**
     * 回滚事务
     */
    public static function rollback()
    {
        self::$instance->rollback();
    }

    /**
     * 提交事务
     */
    public function commit()
    {
        self::$instance->commit();
    }
}