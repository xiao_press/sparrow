<?php
namespace vendor\cache;

abstract class CacheInterface
{
    protected $configs = [];

    public function __construct($configs = null)
    {
        $this->configs = $configs;
    }

    abstract function write($key, $value = null, $expire = 0);

    abstract function get($key);

    abstract function has($key):bool;

}