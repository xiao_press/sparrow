<?php

namespace vendor\cache;

use vendor\cache\CacheInterface;
use Redis;

class RedisImpl extends CacheInterface
{

    /**
     * @var CacheInterface|null
     */
    protected $instance = null;

    /**
     * RedisImpl constructor.
     * @param null $configs
     */
    public function __construct($configs = null)
    {
        parent::__construct($configs);
        $this->instance = new Redis();
        $this->instance->pconnect($configs['HOST'], $configs['PORT'], $configs['TIMEOUT']);
    }

    /**
     * 写入缓存
     * @param $key
     * @param null $value
     * @param int $expire
     */
    public function write($key, $value = null, $expire = 0)
    {
        if($expire === 0){
            $expire = $this->configs['EXPIRE'];
        }

        $this->instance->set($key, $value, $expire);
    }

    /**
     * 读取缓存
     * @param $key
     * @return bool|string
     */
    public function get($key)
    {
        return $this->instance->get($key);
    }

    /**
     * 返回缓存是否存在
     * @param $key
     * @return bool
     */
    public function has($key): bool
    {
        return $this->instance->exists($key);
    }

    /**
     * 回收资源，断开连接
     */
    public function __destruct()
    {
        $this->instance->close();
    }

}