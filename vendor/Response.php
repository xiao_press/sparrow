<?php

namespace vendor;

trait Response
{

    /**
     * 返回 JSON 格式响应
     * @param mixed $data
     * @return false|string
     */
    public static function json($data)
    {
        header('Content-Type:application/json; charset=UTF-8');
        return json_encode($data);
    }

    /**
     * 返回 HTML 格式响应
     * @param mixed $data
     * @return mixed
     */
    public static function html($data)
    {
        header('Content-Type:text/html; charset=UTF-8');
        return $data;
    }
}