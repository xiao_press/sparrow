<?php

namespace vendor;

class Log
{

    const TYPE_NOTICE = 'notice';
    const TYPE_DEBUG = 'debug';
    const TYPE_ERROR = 'error';
    const TYPE_INFO = 'info';

    protected static $logDir = '/var/log/app';

    /**
     * 写入日志
     * @param string|array $message
     * @param string $category
     * @param string $type
     */
    public static function write($message, $category, $type = self::TYPE_INFO)
    {
        is_array($message) && $message = json_encode($message);
        $logFilePath = self::$logDir
            . DIRECTORY_SEPARATOR . date('Y-m-d') . '_' . $type . '.log';
        $logData = '[' . date('Y-m-d H:i:s') . '][' . $category . '] ' . $message . PHP_EOL;
        file_put_contents($logFilePath, $logData, FILE_APPEND);
    }
}