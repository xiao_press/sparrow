<?php
namespace vendor;

class Config
{

    protected static $configs = [];

    /**
     * 加载配置
     * @param array $configs
     */
    public static function load(array $configs)
    {
        self::$configs = array_merge(self::$configs, $configs);
    }

    /**
     * 获取配置
     * @param null|string $key
     * @return null|array
     */
    public static function get($key = null)
    {
        if(!is_null($key) && isset(self::$configs[$key])){
            return self::$configs[$key];
        }

        return self::$configs;
    }

}