<?php

namespace vendor;

trait Request
{

    /**
     * 获取 GET 请求参数
     * @param null|string $key
     * @return mixed
     */
    public static function get($key = null)
    {
        if(!is_null($key) && isset($_GET[$key])){
            return $_GET[$key];
        }
        unset($_GET['s']);

        return $_GET;
    }

    /**
     * 获取 POST 请求参数
     * @param null|string $key
     * @return mixed
     */
    public static function post($key = null)
    {
        if(!is_null($key) && isset($_POST[$key])){
            return $_POST[$key];
        }

        return $_POST;
    }

    /**
     * 获取 GET 和 POST 请求参数
     * @param null|string $key
     * @return array
     */
    public static function params($key = null)
    {
        $params = array_merge($_GET, $_POST);
        if(!is_null($key) && isset($params[$key])){
            return $params[$key];
        }
        unset($params['s']);

        return $params;
    }
}