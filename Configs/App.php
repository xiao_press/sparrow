<?php

return [
    // APP 版本
    'APP_VERSION' => '0.1',
    // 自定义路由
    'CUSTOM_ROUTE' => false,
    // 禁止访问的模块
    'DENY_ACCESS_MODULE' => ['Common', 'Enum'],
    // 缓存配置
    'CACHE' => include '../Configs/Cache.php',
    // 会话配置
    'SESSION' => include '../Configs/Session.php',
    // 数据库配置
    'DATABASE' => include '../Configs/Database.php',
];