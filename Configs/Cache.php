<?php

return [
    'TYPE' => 'Redis',
    'HOST' => '127.0.0.1',
    'PORT' => '6379',
    'EXPIRE' => 7200,
    'TIMEOUT' => 2,
];