<?php

return [
    'PREFIX' => '',         // 前缀
    'EXPIRE' => 3600,       // 过期时间
    'TYPE' => 'File',       // 驱动类型
    'PATH' => '/var/app/session'        // SESSION 的存储目录
];