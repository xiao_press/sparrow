<?php

class SparrowEvent
{

    /**
     * 框架启动事件
     */
    public static function start()
    {
        date_default_timezone_set('Asia/Shanghai');
    }

    /**
     * 解析路由之前
     * @param $uri
     */
    public static function routeDispatchFirst($uri)
    {

    }

    /**
     * 应用结束
     * @param $response
     */
    public static function shutdown($response)
    {

    }
}