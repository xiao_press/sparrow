## 用户表
drop table if exists `user`;
create table if not exists `user`(
  `user_id` bigint auto_increment not null comment '用户ID',
  `name` varchar(32) not null default '' comment '用户名',
  `phone` bigint not null comment '手机号',
  `password` varchar(128) not null comment '密码',
  `last_login_time` datetime not null default '1970-01-01 08:00:00' comment '最近登录时间',
  `last_login_ip` int unsigned not null default 0 comment '最近登录IP',
  `status` tinyint not null default 0 comment '状态:0-正常1-冻结',
  `create_time` datetime default CURRENT_TIMESTAMP comment '创建时间',
  `update_time` datetime default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP comment '更新时间',
  primary key(`user_id`)
) engine = 'InnoDB' default charset 'utf8mb4' comment '用户表';

# 用户信息表
drop table if exists `user_info`;
create table if not exists `user_info`(
  `user_info_id` bigint auto_increment not null comment 'ID',
  `user_id` bigint not null comment '用户ID',
  `real_name` varchar(32) not null default '' comment '真实名称',
  `id_card_no` varchar(24) not null default '' comment '身份证号',
  `birthday` datetime not null default '1970-01-01 08:00:00' comment '出生日期',
  `create_time` datetime not null default current_timestamp comment '创建日期',
  `update_time` datetime not null default current_timestamp on update current_timestamp comment '更新日期',
  primary key (`user_info_id`)
) engine = 'InnoDB' default charset 'utf8mb4' comment '用户表';

## 系统设置表
drop table if exists `setting`;
create table if not exists `setting`(
  `setting_id` bigint auto_increment not null comment 'ID',
  `key` varchar(128) not null comment '设置项',
  `desc` varchar(256) not null default '' comment '描述',
  `content` varchar(2048) not null default '' comment '内容',
  `status` tinyint not null default 0 comment '状态:0-有效1-无效',
  `create_time` datetime not null default current_timestamp comment '创建时间',
  `update_time` datetime not null default current_timestamp on update current_timestamp comment '创建时间',
  primary key (`setting_id`)
) engine = 'InnoDB' default charset 'utf8mb4' comment '系统设置表';