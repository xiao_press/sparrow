<?php
namespace App\System\Controller;

use App\Common\Base\ApiController;
use App\System\Service\AppInfoService;

class App extends ApiController
{

    public function info()
    {
        $result = AppInfoService::getVersion();

        return self::response($result);
    }
}