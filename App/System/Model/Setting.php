<?php
namespace App\System\Model;

use vendor\Model;

class Setting extends Model
{
    // 状态
    const STATUS_VALID = 0;     // 有效
    const STATUS_UNSET = 1;     // 无效

    protected static $table = 'setting';
}