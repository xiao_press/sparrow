<?php
namespace App\System\Service;

use App\Common\Helper\CacheHelper;
use App\Enum\CacheKeysEnum;
use App\Enum\SettingKeysEnum;
use App\System\Model\Setting;

class AppInfoService
{

    /**
     * 获取版本
     */
    public static function getVersion()
    {
        $result = CacheHelper::remember(CacheKeysEnum::APP_VERSION, function () {
            return Setting::getInstance()
                ->where(['status', '=', Setting::STATUS_VALID])
                ->andWhere(['key', '=', SettingKeysEnum::APP_VERSION])
                ->find();
        }, 120, true);

        return $result;
    }
}