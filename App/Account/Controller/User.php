<?php
namespace App\Account\Controller;

use App\Common\Base\ApiController;
use vendor\Db;

class User extends ApiController
{

    public function hello()
    {
        $result = Db::getInstance()->table('user')->join('user_info',
            'user_info.user_id = user.user_id', 'INNER')->find();

        return self::json($result);
    }
}
