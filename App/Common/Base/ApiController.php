<?php
namespace App\Common\Base;

use vendor\Controller;

class ApiController extends Controller
{

    public static function error($code, $message)
    {
        return self::json(compact('code', 'message'));
    }

    public static function response($data = [], $code = 0, $message = 'SUCCESS')
    {
        return self::json(compact('code', 'message', 'data'));
    }
}